const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/User");
const productRoutes = require("./routes/Product");

const app = express();

mongoose.connect("mongodb+srv://Prashant_R:TeaLXbXdHCFwzfo6@zuitt-bootcamp.yuxjdq2.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users",userRoutes);
app.use("/products",productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});

