const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})

	return newUser.save().then((user,error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else {
			return false;
		}
	})
}


module.exports.userAuth = (reqBody) => {
		return User.findOne({email : reqBody.email}).then(result => {
			if(result == null){
				return false
			}
			{
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
					return { access : auth.createAccessToken(result)}
				}
				else{
					return false;
				}
			}
		})
}

module.exports.getUserDetails = (data) => {
	return User.findById(data.userId).then(result => {

			// Changes the value of the user's password to an empty string when returned to the frontend
			// Not doing so will expose the user's password which will also not be needed in other parts of our application
			// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
			result.password = "";

			// Returns the user information with the password as an empty string
			return result;

		});

}


module.exports.createOrder = (data) => {
	
	let newOrder = new Order({
		userId : data.userId,
		totalAmount : data.totalAmount
	})

	for( let i=0; i < data.products.length; i++){
	newOrder.products.push({productId : data.products[i].id}),
	newOrder.products.push({quantity : data.products[i].quantity})

	}

	return newOrder.save().then((order,error) => {
		if(error){
			return false;
		}
		else{ 
			return true;
		}
	})
}


module.exports.getAllOrders = (data) => {
	if(data){
		return Order.find({}).then(result => {
			return result;

		})
	}
	else{
		return false;

	}
}

module.exports.getMyOrders = (data) => {
	if(data.isAdmin){
		return false;
	}
	else {
	return Order.find({userId : data.userId}).then(result => {
		return result;
	})
}
}

module.exports.setAsAdmin = (data) => {
	if(data.isAdmin){
		let updateAdmin = {
			isAdmin : true
		}

		return User.findByIdAndUpdate(data.userId, updateAdmin).then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	}
	else{
		return false;
	}
}
